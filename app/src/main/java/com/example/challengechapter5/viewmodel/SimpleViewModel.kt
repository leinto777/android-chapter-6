package com.example.challengechapter5.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.example.challengechapter5.database.keranjang.Keranjang
import com.example.challengechapter5.database.profile.Profile
import com.example.challengechapter5.model.DataListMenu
import com.example.challengechapter5.model.OrdersData
import com.example.challengechapter5.repository.SimpleRepository
import com.example.challengechapter5.util.Resource
import kotlinx.coroutines.Dispatchers

class SimpleViewModel(private val repository: SimpleRepository) : ViewModel() {
    //Cart
    val orderSuccess = repository.orderSuccess
    val counter = repository.counter
    val items = repository.items
    fun initSelectedItem(data: DataListMenu) = repository.initSelectedItem(data)
    fun incrementCount() = repository.incrementCount()
    fun decrementCount() = repository.decrementCount()
    fun getAllItemCart() = repository.getAllCartItems()
    fun deleteitemById(id: Int) = repository.deleteById(id)
    fun insertCart(note: String) = repository.insertData(note)
    fun increment(keranjang: Keranjang) = repository.increment(keranjang)
    fun decrement(keranjang: Keranjang) = repository.decrement(keranjang)
    fun postData(ordersData: OrdersData) = repository.postData(ordersData)

    //Profile
    fun getProfile(email: String) = repository.getProfileUser(email)
    fun updateProfileUser(profile: Profile) = repository.updateProfileUser(profile)

    // API
    fun getAllCategory() = liveData(Dispatchers.IO) {
        try {
            emit(Resource.success(data = repository.getCategory()))
        } catch (e: Exception) {
            emit(Resource.error(data = null, message = e.message ?: "Error Occurred!"))
        }
    }

    fun getAllMenu() = liveData(Dispatchers.IO) {
        try {
            emit(Resource.success(data = repository.getListMenu()))
        } catch (e: Exception) {
            emit(Resource.error(data = null, message = e.message ?: "Error Occurred!"))
        }
    }


}