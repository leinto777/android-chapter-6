package com.example.challengechapter5.model

data class OrdersData(val username: String, val total: Int, val orders: List<ItemOrder>)

data class ItemOrder(val nama: String, val qty: Int, val catatan: String?, val harga: Int)