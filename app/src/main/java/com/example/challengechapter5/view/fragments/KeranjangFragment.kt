package com.example.challengechapter5.view.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.challengechapter5.R
import com.example.challengechapter5.adapter.AdapterKeranjang
import com.example.challengechapter5.databinding.FragmentKeranjangBinding
import com.example.challengechapter5.viewmodel.SimpleViewModel
import org.koin.android.ext.android.inject

class KeranjangFragment : Fragment() {
    private var _binding: FragmentKeranjangBinding? = null
    private val binding get() = _binding!!
    private lateinit var adapterKeranjang: AdapterKeranjang
    private val viewModel: SimpleViewModel by inject()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentKeranjangBinding.inflate(inflater, container, false)

        fetchData()

        binding.pesanBtnKeranjang.setOnClickListener {
            findNavController().navigate(R.id.action_keranjangFragment_to_confirmOrderFragment)
        }

        toConfirmOrder()
        return(binding.root)
    }

    private fun fetchData() {
        adapterKeranjang = AdapterKeranjang(viewModel)
        binding.rvKeranjang.setHasFixedSize(true)
        binding.rvKeranjang.layoutManager = LinearLayoutManager(requireContext())
        binding.rvKeranjang.adapter = adapterKeranjang
        viewModel.getAllItemCart().observe(viewLifecycleOwner) {
            if (it.isEmpty()) {
                binding.rvKeranjang.visibility = View.GONE
                binding.emptyImage.visibility = View.VISIBLE
                binding.tvEmptyCart.visibility = View.VISIBLE

                binding.tvTotalKeranjang.text = "0"
            } else {
                adapterKeranjang.setData(it)
                var totalPrice = 0
                it.forEach { item ->
                    totalPrice += item.totalPrice!!
                }
                val priceText = "Rp. $totalPrice"

                binding.tvTotalKeranjang.text = priceText
            }
        }
    }

    private fun toConfirmOrder() {
        binding.pesanBtnKeranjang.setOnClickListener {
            if (binding.tvTotalKeranjang.text.toString() == "Rp. 0" ) {
                binding.pesanBtnKeranjang.isEnabled = false
                Toast.makeText(requireContext(), "Keranjang kosong", Toast.LENGTH_SHORT).show()
            } else {
                binding.pesanBtnKeranjang.isEnabled = true
                findNavController().navigate(R.id.action_keranjangFragment_to_confirmOrderFragment)
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}