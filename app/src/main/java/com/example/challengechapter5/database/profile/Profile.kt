package com.example.challengechapter5.database.profile

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.parcelize.Parcelize

@Parcelize
@Entity(tableName = "profile")

data class Profile(
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0,
    @ColumnInfo(name = "alamat_user")
    var alamat: String,
    @ColumnInfo(name = "email_user")
    var email: String,
    @ColumnInfo(name ="mobile_user")
    var mobile: String?,
): Parcelable