@file:Suppress("unused")

package com.example.challengechapter5.database.keranjang

import android.app.Application
import androidx.lifecycle.LiveData
import com.example.challengechapter5.util.Callback
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

@Suppress("unused")
class KeranjangRepo(application: Application) {

    private var _keranjangDAO: KeranjangDAO
    private val executorService: ExecutorService = Executors.newSingleThreadExecutor()

    init {
        val db = KeranjangDatabase.getInstance(application)
        _keranjangDAO = db.keranjangDAO()
    }

    fun getAllCartItems(): LiveData<List<Keranjang>> = _keranjangDAO.getAllItems()

    fun deleteItemCart(itemId: Int) {
        executorService.execute {
            _keranjangDAO.deleteItemById(itemId)
        }
    }

    fun insertData(keranjang: Keranjang) {
        executorService.execute { _keranjangDAO.insert(keranjang) }
    }

    fun updateQuantityItem(keranjang: Keranjang) {
        executorService.execute { _keranjangDAO.update(keranjang) }
    }

    fun deleteAll() {
        executorService.execute { _keranjangDAO.delete() }
    }

    fun getItem(foodName: String, callback: Callback) {

        executorService.execute {
            val keranjang = _keranjangDAO.getItemLive(foodName)
            callback.onKeranjangLoaded(keranjang)
        }
    }
}