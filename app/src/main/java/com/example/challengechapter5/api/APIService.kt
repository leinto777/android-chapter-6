package com.example.challengechapter5.api

import com.example.challengechapter5.model.CategoryMenuResponse
import com.example.challengechapter5.model.ListMenuResponse
import com.example.challengechapter5.model.OrderResponse
import com.example.challengechapter5.model.OrdersData
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface APIService {
    @GET("listmenu")
    suspend fun getListMenu(): ListMenuResponse

    @GET("category-menu")
    suspend fun getCategoryMenu(): CategoryMenuResponse

    @POST("order-menu")
    fun postOrder(@Body orderData: OrdersData): Call<OrderResponse>


}